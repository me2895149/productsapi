import { jest } from '@jest/globals'
import { pool } from '../src/db/db'
import server from '../src/server'
import request from 'supertest'

const express = require('express')
const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

describe('Testing GET /products', () => {
    const mockResponse = {
        rows: [
            { id: '277c9f32-f7fa-4ecc-8cce-eadcb1227391', name: 'new product', price: 350 }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    test('Testin GET /products', async () => {
        const response = await request(server).get('/products')
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })
})
