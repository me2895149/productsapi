import { Router, Request, Response } from 'express'
import dao from './db/productDao'

const router = Router()


//Findall
router.get('/', async (req: Request, res: Response) => {
	const result = await dao.findAll()
	res.send(result.rows)   
})

//Find by id
router.get('/:id', async (req: Request, res: Response) => {
	const result = await dao.findOne(req.params.id)
	const product = result.rows[0]
	res.send(product)   
})

//Add new product
router.post('/', async (req: Request, res: Response) => {
	const product = req.body
	const result = await dao.insertProduct(product)
	const storedProduct = { id: result.rows[0], ...product}
	res.send(storedProduct)
})

//Modify product
router.put('/:id', async (req: Request, res: Response) => {
	const product = { id: req.params.id, ...req.body}
	await dao.updateProduct(product)
	res.send(product)
})

//Delete product
router.delete('/:id', async (req: Request, res: Response) => {
	const id = req.params.id
	await dao.deleteById(id)
	res.status(200).send('Deleted')
})

export default router