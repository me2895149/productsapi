
import { createProductsTable } from './db/db'
import server from './server'

createProductsTable()

const { PORT } = process.env
server.listen(PORT, () => {
	console.log('Products API listening to port', PORT)
})
