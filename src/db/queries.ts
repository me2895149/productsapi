const findAll = 'SELECT * FROM products;'
const insertProduct = 'INSERT INTO products (id, name, price) VALUES ($1, $2, $3);'
const findOne = 'SELECT * FROM products WHERE id = $1;'
const updateProduct = 'UPDATE products SET name = $2, price = $3 WHERE id = $1;'
const deleteById = 'DELETE FROM products WHERE id = $1;'

export default { findAll, insertProduct, findOne, updateProduct, deleteById }
