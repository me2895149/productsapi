import { v4 as uuidv4 } from 'uuid'
import  { executeQuery } from './db'
import queries from './queries'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const insertProduct = async (product: any) => {
	const id = uuidv4()
	const params = [id, ...Object.values(product)]
	console.log(`Inserting a new product ${params[0]}...`)
	const result = await executeQuery(queries.insertProduct,params)
	console.log(`New product ${id} inserted succesfully.`)
	return result
}

const findAll = async () => {
	console.log('Requesting for all products...')
	const result = await executeQuery(queries.findAll)
	console.log(`Found ${result.rows.length} products.`)
	return result
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const findOne = async (id: any) => {
	console.log(`Requesting a product tiwh id:${id}...`)
	const result = await executeQuery(queries.findOne, [id])
	console.log(`Found ${result.rows.length} products.`)
	return result
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const updateProduct = async (product: any) => {
	const params = [product.id, product.name, product.price]
	console.log(`Updatin a product ${params[0]}...`)
	const result = await executeQuery(queries.updateProduct, params)
	console.log(`Product ${product.id} updated succesfully.`)
	return result
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const deleteById = async (id: any) => {
	console.log(`Deleting product tiwh id:${id}`)
	const params = [id]
	const result = await executeQuery(queries.deleteById, params)
	return result
}

export default { findAll, insertProduct, findOne, updateProduct, deleteById }